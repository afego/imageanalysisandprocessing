import numpy as np
import cv2 as cv

'''
Arquivo para transformacao do espaco de cor da imagem
'''

def rgb_median(image, save_path:str = None):
    '''
    Teste de imagem grayscale como mediana
    '''
    height, width, channels = image.shape #  num of lines, num of columns, depth
    median = np.zeros((height, width))
    for i in range(height):
        for j in range(width):
            for k in range(channels):
                median[i][j] += image[i][j][k]
            median[i][j] = median[i][j]/channels

    if save_path:
        cv.imwrite(save_path, median)
    return median

def main():

    image_folder = "imagens/"
    output_folder = "output/colourspace/"
    filename = "dive4_F3_master.mp4-00_00_00-00001"
    file_format = ".png"
    
    image = cv.imread(image_folder+filename+file_format) #  BGR

    grayscale = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
    cv.imwrite(output_folder+filename+"_gray"+file_format, grayscale)
    #median = rgb_median(image, save_path=output_folder+filename+"_rgbmedian"+file_format)
    dictionary = {
        "hsv":cv.COLOR_BGR2HSV,
        "hls":cv.COLOR_BGR2HLS,
        "lab":cv.COLOR_BGR2LAB,
        "luv":cv.COLOR_BGR2LUV,
        "xyz":cv.COLOR_BGR2XYZ,
        "ycrcb":cv.COLOR_BGR2YCrCb 
    }
    for key in dictionary:
        img = cv.cvtColor(image, dictionary[key])
        cv.imwrite(output_folder+filename+"_"+key+file_format, img)
        shape = img.shape
        if len(shape) == 3:
            for i in range(shape[2]):
                cv.imwrite(output_folder+filename+"_"+key+"_"+str(i)+file_format, img[:,:,i])
    return

if __name__ == "__main__":
    main()