from typing import List
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import copy
import sys
import raves_morph_transf as raves
        
def my_resize(image, prct):
    rows, cols = image.shape
    new_rows = int(prct*rows)
    new_cols = int(prct*cols)
    new_dim = (new_cols, new_rows)
    new_image = cv.resize(image, new_dim)
    return new_image

def my_dft(image): #  https://www.youtube.com/watch?v=x7FPKU97fgs
    complex_mat = [np.float32(image), np.zeros(image.shape, np.float32)]
    complex_image = cv.merge(complex_mat)
    dft_image = complex_image
    dft_flags = cv.DFT_COMPLEX_OUTPUT
    cv.dft(complex_image, dft_image, flags=dft_flags)
    dft_image = np.fft.fftshift(dft_image)
    return dft_image

def show_dft(image):
    size = (image.shape[0], image.shape[1])
    plane = [np.zeros(size, np.float32), np.zeros(size, np.float32)]
    cv.split(image, plane) #  plane[0] = Re(FT), plane[1] = Im(FT)
    magnitude = np.zeros(size, np.float32) #  Matrix with same size as plane[0] to receive plane's magnitude
    cv.magnitude(plane[0], plane[1], magnitude)

    mat_ones = np.ones(size, dtype=magnitude.dtype) #  Matrix to switch magnitude to logharitmic scale
    cv.add(mat_ones, magnitude, magnitude)
    cv.log(magnitude, magnitude)
    cv.normalize(magnitude, magnitude, 0, 1, cv.NORM_MINMAX)
    # magnitude = recenter_dft(magnitude) #  Used if my_dft doesnt apply fftshift

    # cv.imshow("Magnitude", magnitude)
    # cv.waitKey()
    # plt.imshow(magnitude)
    # plt.show()
    return magnitude

def recenter_dft(image): #  Function for visualiation purposes only
    magnitude = image
    magnitude_rows, magnitude_cols = magnitude.shape
    # crop the spectrum, if it has an odd number of rows or columns
    #magnitude = magnitude[0:(magnitude_rows & -2), 0:(magnitude_cols & -2)]
    cx = int(magnitude_rows/2)
    cy = int(magnitude_cols/2)
    q0 = magnitude[0:cx, 0:cy]         # Top-Left - Create a ROI per quadrant
    q1 = magnitude[cx:cx+cx, 0:cy]     # Top-Right
    q2 = magnitude[0:cx, cy:cy+cy]     # Bottom-Left
    q3 = magnitude[cx:cx+cx, cy:cy+cy] # Bottom-Right
    tmp = np.copy(q0)               # swap quadrants (Top-Left with Bottom-Right)
    magnitude[0:cx, 0:cy] = q3
    magnitude[cx:cx + cx, cy:cy + cy] = tmp
    tmp = np.copy(q1)               # swap quadrant (Top-Right with Bottom-Left)
    magnitude[cx:cx + cx, 0:cy] = q2
    magnitude[0:cx, cy:cy + cy] = tmp

    return magnitude

def invert_dft(image): #  https://stackoverflow.com/questions/59975604/how-to-inverse-a-dft-with-magnitude-with-opencv-python
    inverse = copy.deepcopy(image)
    dft_flags = cv.DFT_INVERSE + cv.DFT_COMPLEX_OUTPUT
    cv.dft(inverse,inverse, flags=dft_flags)
    inverse = cv.magnitude(inverse[:,:,0], inverse[:,:,1])
    inverse = cv.normalize(inverse, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8U)
    inverse = cv.rotate(inverse, rotateCode=cv.ROTATE_180)
    return inverse

def circular_area(size, radius_perct = 10):
    """
    Circular area used for defining if its area will be used as HPF or LPF
    """
    rows, cols = size[0], size[1]
    crow, ccol = int(rows/2), int(cols/2)
    if rows > cols:
        highest_value = rows
    else:
        highest_value = cols
    radius = int(radius_perct*highest_value/100)
    center = [crow,ccol]
    x,y = np.ogrid[:rows,:cols]
    area = (x-center[0])**2 + (y-center[1])**2 <= radius*radius
    return area

def circular_mask(size, radius_perct = 10):
    """
    Circular mask for high pass filter
    rows, cols = size
    """
    mask = np.ones((size[0], size[1], 2), np.uint8) #  Creating a mask filled with 1
    mask_area = circular_area(size, radius_perct)
    mask[mask_area] = 0
    return mask

def ring_mask(size, inner_perct = 10, outer_perct = 50):
    mask = np.ones((size[0], size[1], 2), np.uint8) #  Creating a mask filled with 1
    mask_inner = circular_area(size, inner_perct)
    mask_outer = circular_area(size, outer_perct)
    mask[mask_outer] = 0
    mask[mask_inner] = 1
    return mask

def diagonal_mask(size, ang_coef = 1, ang_coef2 = 1):
    rows, cols = size
    crow, ccol = int(rows/2), int(cols/2)
    mask = np.ones((rows,cols, 2), np.uint8) #  Creating a mask filled with 1
    center = [crow,ccol]
    x,y = np.ogrid[:rows,:cols]
    mask_area = (
                ((y-center[1]) - (x-center[0])*ang_coef > 0) & ((y-center[1]) - (x-center[0])*ang_coef2 < 0) | 
                ((y-center[1]) - (x-center[0])*ang_coef < 0) & ((y-center[1]) - (x-center[0])*ang_coef2 > 0) 
                )
    #mask_area2 = ((y-center[1]) - (x-center[0])*ang_coef < 0) & ((y-center[1]) - (x-center[0])*ang_coef2 > 0)
    mask[mask_area] = 0
    #mask[mask_area2] = 0

    return mask

def axis_mask(size, thickness = 3, perct_x = 100, perct_y = 100):
    """""
    Mask that covers only the center-most horizontal and vertical lines
    perct_x and perct_y indicates how much of the horizontal and vertical line you want to use, respectively
    """""
    rows, cols = size
    crow, ccol = int(rows/2), int(cols/2)
    x_limit, y_limit = int(ccol*perct_x/100), int(crow*perct_y/100)
    mask = np.ones((rows,cols, 2), np.uint8) #  Creating a mask filled with 1
    mask_info = "_axis_t"+str(thickness)+"_x"+str(perct_x)+"_y"+str(perct_y)
    for i in range(len(mask)):
        for j in range(len(mask[i])):
            if ((i == crow) or ( i < crow+thickness and i > crow-thickness) ) and ( j > ccol-x_limit and j < ccol+x_limit ):
                mask[i][j] = 0
            if ((j == ccol) or ( j < ccol+thickness and j > ccol-thickness) ) and ( i > crow-y_limit and i < crow+y_limit ):
                mask[i][j] = 0
    
    return mask
    
def show_images(images:List):
    #fig = plt.figure(figsize=( int(len(images)/2), len(images) - int(len(images)/2)))
    k = len(images)
    fig = plt.figure(figsize=(k,k))
    i=1
    for image in images:
        ax = fig.add_subplot(int(len(images)/2),2,i)
        ax.imshow(image, cmap='gray')
        ax.title.set_text(str(i))
        i += 1
    plt.show()

def main():
    output_folder = "output/dft/"
    image_folder = "imagens/"
    
    filename = 'dive4_F3_master.mp4-00_00_00-00001_min'
    img_format = ".png"
    sufix = []
    
    orig_image = cv.imread(image_folder+filename+img_format, flags=cv.IMREAD_GRAYSCALE)
    
    #resized_image = my_resize(orig_image, 0.6)
    resized_image = orig_image

    images = [resized_image]

    # Calculando o espectro de Fourier
    freq_domain = my_dft(resized_image)
    # Gerando a magnitude para poder ver o espectro
    images.append( show_dft(freq_domain) )
    cv.imwrite(f"{output_folder}{filename}_FS.tiff", show_dft(freq_domain))

    image_size = resized_image.shape
    # Mascara para a convolucao
    mask = ring_mask(image_size, 5,15)
    # mask = circular_mask(image_size, radius_perct=0.2)
    # Convolucao
    mask_fd = freq_domain * mask
    images.append( show_dft(mask_fd))
    cv.imwrite(f"{output_folder}{filename}_FS_MASK.tiff", show_dft(mask_fd))
    # Saindo do espectro de Fourier para poder visualizar o resultado
    edited_image = invert_dft(mask_fd)
    images.append(edited_image)
    cv.imwrite(f"{output_folder}{filename}_DFT.png", edited_image)
    # show_images(images)

if __name__ == "__main__":
    main()