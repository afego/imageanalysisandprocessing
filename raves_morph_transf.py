import cv2 as cv
import dft
import numpy as np

def my_gradient(image_grayscale, shape=cv.MORPH_ELLIPSE, size=(4,4), iterations=1):
    kernel = cv.getStructuringElement(shape, size)
    return cv.morphologyEx(image_grayscale, cv.MORPH_GRADIENT, kernel, iterations=iterations, borderType=cv.BORDER_DEFAULT)

def my_open(image_greyscale, shape=cv.MORPH_ELLIPSE, size=(3,3), iterations=1):
    kernel = cv.getStructuringElement(shape, size)
    return cv.morphologyEx(image_greyscale, cv.MORPH_OPEN, kernel, iterations=iterations, borderType=cv.BORDER_DEFAULT)

def my_close(image_greyscale, shape=cv.MORPH_ELLIPSE, size=(5,5), iterations=1):
    kernel = cv.getStructuringElement(shape, size)
    return cv.morphologyEx(image_greyscale, cv.MORPH_CLOSE, kernel, iterations=iterations, borderType=cv.BORDER_DEFAULT)

def my_morph(image_greyscale, op, shape=cv.MORPH_ELLIPSE, size=(5,5), iterations=1):
    kernel = cv.getStructuringElement(shape, size)
    return cv.morphologyEx(image_greyscale, op, kernel, iterations=iterations)

def my_thresh(image_greyscale, thresh=10, maxval=255, type=cv.THRESH_BINARY_INV):
    return cv.threshold(image_greyscale, thresh, maxval, type)

def my_grayscale(image):
    shape = image.shape
    image_grayscale = image
    if len(shape)>2:
        image_grayscale = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    return image_grayscale

def transformation(image_greyscale):
    '''
    Atual transformacoes utilizadas no RAVES
    '''
    kernel1 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    #kernel2 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (4,4), (-1,-1))
    kernel3 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5,5), (-1,-1))
    #image_grad = cv.morphologyEx(image_greyscale, cv.MORPH_GRADIENT, kernel2, iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_grad = my_gradient(image_greyscale, cv.MORPH_ELLIPSE, size=(4,4), iterations=1)
    _, image_bin = cv.threshold(image_grad, thresh=10, maxval=255, type=cv.THRESH_BINARY_INV)
    image_open = cv.morphologyEx(image_bin, cv.MORPH_OPEN, kernel1, anchor=(-1,-1), iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_close = cv.morphologyEx(image_open, cv.MORPH_CLOSE, kernel3, anchor=(-1,-1), iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    return image_close

def transformation_niblack(image_greyscale, max_value:int=255, block_size:int=3, k:float=0):
    kernel1 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    kernel2 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (4,4), (-1,-1))
    kernel3 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5,5), (-1,-1))
    image_grad = cv.morphologyEx(image_greyscale, cv.MORPH_GRADIENT, kernel2, iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_bin = cv.ximgproc.niBlackThreshold(
                _src=image_grad,
                maxValue=max_value,
                type=cv.THRESH_BINARY,
                blockSize=block_size,
                k=k
            )
    image_open = cv.morphologyEx(image_bin, cv.MORPH_OPEN, kernel1, anchor=(-1,-1), iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_close = cv.morphologyEx(image_open, cv.MORPH_CLOSE, kernel3, anchor=(-1,-1), iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    return image_close

def transformation_test(image_greyscale, max_value:int=255, block_size:int=3, k:float=0):
    kernel_gradient = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    kernel_erode = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2,2), (-1,-1))
    kernel_dilation = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2,2), (-1,-1))
    kernel_opening = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    image_open = cv.morphologyEx(image_greyscale, cv.MORPH_OPEN, kernel_opening, iterations=1)
    image_grad = cv.morphologyEx(image_open, cv.MORPH_GRADIENT, kernel_gradient, iterations=2, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_bin = cv.ximgproc.niBlackThreshold(_src=image_grad, maxValue=max_value, type=cv.THRESH_BINARY, blockSize=block_size, k=k)
    image_erod = cv.morphologyEx(image_bin, cv.MORPH_ERODE, kernel_erode, iterations=1)
    #image_dila = cv.morphologyEx(image_erod, cv.MORPH_DILATE, kernel_dilation, iterations=1)
    #image_open = cv.morphologyEx(image_bin, cv.MORPH_OPEN, kernel_opening, iterations=1)
    image = image_erod
    return image

def transformation_dft(image_greyscale, max_value:int=255, block_size:int=3, k:float=0):
    kernel_gradient = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    kernel_opening = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3,3), (-1,-1))
    image_size = image_greyscale.shape
    freq_domain = dft.my_dft(image_greyscale)
    # cv.imshow("DFT",dft.show_dft(freq_domain))
    # cv.waitKey()
    mask = dft.circular_mask(image_size, radius_perct=1)
    idft = dft.invert_dft(freq_domain * mask)
    cv.imshow("idft",idft)
    cv.waitKey()
    image_grad = cv.morphologyEx(idft, cv.MORPH_GRADIENT, kernel_gradient, iterations=1, borderType=cv.BORDER_DEFAULT, borderValue=(1,0))
    image_open = cv.morphologyEx(image_grad, cv.MORPH_OPEN, kernel_opening, iterations=1)
    image_bin = cv.ximgproc.niBlackThreshold(_src=image_open, maxValue=max_value, type=cv.THRESH_BINARY, blockSize=block_size, k=k)
    image = image_bin
    return image

def full_transformation(image):
    image_greyscale = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    image_greyscale = my_gradient(image_greyscale)
    _, image_greyscale = my_thresh(image_greyscale)
    image_greyscale = my_open(image_greyscale)
    image_greyscale = my_close(image_greyscale)
    return image_greyscale

def full_dft(image):
    image_size = image.shape
    if len(image_size) >2:
        image_grayscale = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    else:
        image_grayscale = image
    freq_domain = dft.my_dft(image_grayscale)
    mask = dft.circular_mask(image_size[:2], radius_perct=0.1)
    #mask = dft.axis_mask(image_size[:2], perct_x=30, perct_y=30)
    idft = dft.invert_dft(freq_domain*mask)
    return idft

def pre_watershed(image):
    image = full_dft(image)
    image = my_gradient(image)
    #_, bin_image = my_thresh(image_grad, thresh=0)
    _, image = cv.threshold(image, thresh=0, maxval= 255, type=cv.THRESH_BINARY + cv.THRESH_OTSU)

    image = my_open(image, size=(2,2), iterations=1)

    return image

def canny_detection(image):
    return cv.Canny(
        image,
        100, #  minVal
        200, #  maxVal
        apertureSize=3 #  3 by default
    )

def sobel_detection(image):
    return cv.Sobel(
        image,
        ddepth=cv.CV_64F,
        dx=1,
        dy=1,
        ksize=3
    )

def hough_transform(image):
    image_grayscale = my_grayscale(image)
    
    image_grayscale = cv.medianBlur(image_grayscale, ksize=5)
    rows = image_grayscale.shape[0]
    circles = cv.HoughCircles(
                            image_grayscale, 
                            cv.HOUGH_GRADIENT, 
                            1,  #  inverse ration of resolution
                            rows/8, #  minimun distance between centers
                            param1=100, #  upper threshold for canny edge detection 
                            param2=30,  #  threshold for center detection
                            minRadius=0,    #  minimun radius, 0 by default
                            maxRadius=0)   #  max radius to be detected, 0 by default
    
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            center = (i[0], i[1])
            # circle center
            cv.circle(image, center, 1, (0, 100, 100), 3)
            # circle outline
            radius = i[2]
            cv.circle(image, center, radius, (255, 0, 255), 3)
    return image_grayscale

def draw_contour(image):
    contours, hierarchy = cv.findContours(image, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_SIMPLE)
    rows, cols = image.shape
    background = np.zeros([rows,cols,3], dtype=np.uint8)
    background.fill(255) # or img[:] = 255
    cv.drawContours(background, contours, -1, (0,255,0), 10)
    return background

def frame_subtraction(frame_greyscale1, frame_greyscale2):
    # difference between frames to only show the moving bubbles
    sub1 = cv.absdiff(frame_greyscale1, frame_greyscale2)

    # differences to remove possible noise
    sub2 = cv.absdiff(sub1, frame_greyscale1)
    sub3 = cv.absdiff(sub2, frame_greyscale2)

    result = sub3
    return result

def frame_processing(frame_greyscale1, frame_greyscale2):
    frame_diff = frame_subtraction(frame_greyscale1, frame_greyscale2)
    frame_transf = transformation(frame_diff)
    return frame_transf

def consecutive_frames(video_name, frame_pos=0, frame_dist=0):
    try:
        video = cv.VideoCapture(video_name)
    except:
        print("could not open {}".format(video_name))
        return
    total_frame = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    if frame_pos < total_frame:
        video.set(cv.CAP_PROP_POS_FRAMES, frame_pos)
        success, frame1 = video.read()
        if frame_pos+frame_dist < total_frame:
            video.set(cv.CAP_PROP_POS_FRAMES, frame_pos+frame_dist)
            success, frame2 = video.read()
        else:
            print(f"Second frame frame {frame_pos+frame_dist} is bigger than video frame count {total_frame}")
            return
    else:
        print(f"Starting frame {frame_pos} is bigger than video frame count {total_frame}")
        return
    if not success:
        print("could not read {}".format(video_name))
        return
    return frame1, frame2

def main():
    pass

if __name__ == '__main__':
    main()