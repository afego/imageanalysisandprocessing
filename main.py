import re
import csv
from typing import List
import argparse
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import histogram as hist
import thresholding as thrs
import spacedomain as sd
import dft
import raves_morph_transf as rmt

def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser
    parser.add_argument(
        "image",
        type=str,
        default=None,
        help="Caminho absoluto para a imagem a ser lida"
    )
    return parser

def concatenate_images(img1, img2):
    return np.hstack((img1, img2))

def histogram_func(imagefolder, files, fileformat, outputfolder):
    subfolder = 'histogram/'
    for filename in files:
        sufix = []
        images = []
        image = cv.imread(imagefolder+filename+fileformat) #  BGR

        image_hist = hist.histogram(image)
        sufix.append('_hist')
        images.append(image_hist)

        image_norm = hist.normalize(image)
        sufix.append('_norm')
        images.append(image_norm)
        hist_norm = hist.histogram(image_norm)
        sufix.append('_norm_hist')
        images.append(hist_norm)

        image_eq = hist.equalization(image)
        sufix.append('_eq')
        images.append(image_eq)
        hist_eq = hist.histogram(image_eq)
        sufix.append('_eq_hist')
        images.append(hist_eq)

        #===========

        # clip_limit = 4
        # tile_rows = 50
        # tile_cols = 50
        # tile = (tile_rows,tile_cols)
        # image_clahe = hist.clahe(image, clip_limit, tile)
        # sufix.append(f'_clahe{clip_limit}-{tile_rows}-{tile_cols}')
        # images.append(image_clahe)
        # hist_clahe = hist.histogram(image_clahe)
        # sufix.append(f'_clahe{clip_limit}-{tile_rows}-{tile_cols}_hist')
        # images.append(hist_clahe)

        # hybrid = hist.equalization(image_clahe)
        # sufix.append(f'_hybrid')
        # images.append(hybrid)
        # hist_hybrid = hist.histogram(hybrid)
        # sufix.append(f'_hybrid_hist')
        # images.append(hist_hybrid)

        for id, _ in enumerate(images):
            cv.imwrite(outputfolder+subfolder+filename+sufix[id]+fileformat, images[id])

    return

def spacedomain_func(imagefolder:str, files:List[str], fileformat:str, outputfolder:str, lpf:float=1, hpf:float=1, show_steps=False, show_magnitude=False):
    subfolder = 'spacedomain/'
    
    if fileformat[0] != '.':
        print('fileformat argument starts with a "."')
        return
    for filename in files:
        images = []
        sufix = []
        homomorphic_output = outputfolder+subfolder+filename
        #homomorphic_output = ''
        image = cv.imread(imagefolder+filename+fileformat) #  BGR
        image = cv.cvtColor(image, cv.COLOR_BGR2GRAY) #  Grayscale

        # Homomorphic filtering

        # image_homomorphic = sd.homomorphic(image, lpf_const=lpf, hpf_const=hpf, show_steps=show_steps, show_dft=show_magnitude, file_output=homomorphic_output)
        # sufix.append(f'_homor_l{lpf}_h{hpf}')
        # images.append(image_homomorphic)

        # Marr-Hildreth edge detection
        
        # log, zero_crossing = sd.edgesMarrHildreth(image, 3)
        log, zero_crossing = sd.MarrHildreth(image, 1)
        sufix.append('_marr_log')
        images.append(log)
        sufix.append('_marr_zero')
        images.append(zero_crossing)

        for id, _ in enumerate(images):
            cv.imwrite(outputfolder+subfolder+filename+sufix[id]+fileformat, images[id])
    return

def colourspace_func(imagefolder:str, files:List[str], fileformat:str, outputfolder:str):
    subfolder = 'colourspace/'
    for filename in files:
        output_file = outputfolder+subfolder+filename
        image = cv.imread(imagefolder+filename+fileformat) #  BGR

        grayscale = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
        cv.imwrite(output_file+"_gray"+fileformat, grayscale)
        #median = rgb_median(image, save_path=output_folder+filename+"_rgbmedian"+file_format)
        dictionary = {
            "hsv":cv.COLOR_BGR2HSV,
            "hls":cv.COLOR_BGR2HLS,
            "lab":cv.COLOR_BGR2LAB,
            "luv":cv.COLOR_BGR2LUV,
            "xyz":cv.COLOR_BGR2XYZ,
            "ycrcb":cv.COLOR_BGR2YCrCb 
        }
        for key in dictionary:
            img = cv.cvtColor(image, dictionary[key])
            cv.imwrite(output_file+"_"+key+fileformat, img)
            shape = img.shape
            if len(shape) == 3:
                for i in range(shape[2]):
                    cv.imwrite(output_file+"_"+key+"_"+str(i)+fileformat, img[:,:,i])
        return

def contour_counting(imagefolder:str, files:List[str], fileformat:str, outputfolder:str):
    subfolder = "contours/"
    file_rows = []
    sigma_start = 1
    # sigma_start = 1
    sigma_end = 2
    sigma_step = 0.2
    sigmas = [x for x in np.arange(sigma_start, sigma_end+sigma_step, sigma_step)]
    csv_header = ['RAVES']
    for sigma in sigmas:
        csv_header.append(sigma)
    csv_header.append('Canny')
    thr1_start = 0
    thr1_end = 150
    thr1_step = 30
    thr1_list = [x for x in range(thr1_start, thr1_end+thr1_step, thr1_step)]
    #file_rows.append(thr1_list)
    thr2_start = 200
    thr2_end = 260
    thr2_step = 10
    thr2_list = [x for x in range(thr2_start, thr2_end+thr2_step, thr2_step)]
    #file_rows.append(thr2_list)
    file_rows.append(csv_header)
    for file in files:
        print(f"file {file}")
        row = [] # Line for csv file
        min_size = 6
        max_size = 50
        sufix = []
        images = []
        image_orig = cv.imread(imagefolder+file+fileformat)
        image_gray = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)

        # RAVES
        # for thresh in thr1_list:
        #     for max_val in thr2_list:
        #         images.append(rmt.transformation(image_gray, thresh, max_val))
        #         sufix.append(f'_transf_{thresh}_{max_val}')
        thresh = 10
        max_val = 255
        images.append(rmt.transformation(image_gray))
        sufix.append(f'_transf_{thresh}_{max_val}')
        
        image_norm = hist.normalize(image_gray)
        
        # Marr-Hildreth
        for sigma in sigmas:
            _, zc = sd.edgesMarrHildreth(image_norm, sigma)
            images.append(zc)
            sufix.append(f'_MH{sigma}')

        # Canny
        thr1 = 100
        thr2 = 200
        sufix.append(f'_canny_{thr1}_{thr2}')
        images.append(cv.Canny(image_norm, threshold1=thr1, threshold2=thr2))

        for id, image in enumerate(images):
            contour_list = []
            contours, _ = cv.findContours(image.astype("uint8"), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
            #print(len(contours))
            contour_image = np.zeros((image.shape[0], image.shape[1], 1), dtype="uint8")
            for contour in contours:
                rect = cv.boundingRect(contour) #x, y, w, h
                #width = abs(rect[2] - rect[0])
                #height = abs(rect[3] - rect[1])
                width = rect[2]
                height = rect[3]
                if width >= min_size and width <= max_size and width > height and width < height * 2.2:
                    contour_list.append(contour)
            #   print(len(contour_list))
            contour_tuple = tuple(contour_list)
            #contour_tuple = contours

            cv.drawContours(contour_image, contour_tuple, -1, (255,255,255), 1)
            # print(f'image {id} count: {hist.histogram_count(contour_image.astype("uint8"))}')
            row.append(hist.histogram_255_count(contour_image.astype("uint8")))
            cv.imwrite(outputfolder+subfolder+file+sufix[id]+"_contours"+fileformat, contour_image)
        file_rows.append(row)
    generate_csv(file_rows, outputfolder+subfolder+'255count.csv')
    return

def testing_transformation(imagefolder:str, files:List[str], fileformat:str, outputfolder:str):
    subfolder = 'tcc/'
    for file in files:
        print(f"file {file}")
        images = []
        sufix = []
        image_orig = cv.imread(imagefolder+file+fileformat)
        
        ## Transformacoes atuais
        transf = rmt.transformation(cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY))
        transf = rmt.my_gradient(transf)
        transf = cv.Canny(transf, 155, 200)
        # _, transf = sd.edgesMarrHildreth(transf, 3)
        sufix.append(f'_transf')
        images.append(transf)
        # transf_hist = hist.histogram(transf)
        # sufix.append(f'_trasnf_hist')
        # images.append(transf_hist)
        # print(f'Transf count: {hist.histogram_count(transf.astype("uint8"))}')

        # Passo 1: Obter canal Y
        image_Y = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
        sufix.append(f'_1_Ychannel')
        images.append(image_Y)
        # Passo 2: Normalizacao
        image_norm = hist.normalize(image_Y, alpha=0, beta=255)
        sufix.append(f'_2_norm')
        images.append(image_norm)

        # Passo 3.1: Canny
        # https://docs.opencv.org/3.4/da/d22/tutorial_py_canny.html
        thr1 = 100
        thr2 = 200
        image_can = cv.Canny(image_norm, threshold1=thr1, threshold2=thr2)
        sufix.append(f'_3_A_canny_{thr1}_{thr2}')
        images.append(image_can)
        # print(f'Canny count: {hist.histogram_count(image_can)}')

        # Passo 3.2: Marr-Hildreth
        #sigma = 5
        for sigma in np.arange(1,1.4,0.1):
            _, image_zc = sd.edgesMarrHildreth(image_norm, sigma)
            sufix.append(f'_3_B_MH{sigma}')
            images.append(image_zc)
            # zc_hist = hist.histogram(image_zc)
            # sufix.append(f'_MH{sigma}_hist')
            # images.append(zc_hist)
            # print(f'MH{sigma} count: {hist.histogram_count(image_zc.astype("uint8"))}')

        # Passo 3.3: Homeomorfico com Gaussiano e Laplaciano
        lpf = 0
        hpfs = [x for x in range(1,6)]
        for hpf in hpfs:
            image_hom = sd.homomorphic(image_norm, lpf_const=lpf, hpf_const=hpf)
            # _, image_hom = cv.threshold(image_hom, 0, 255, cv.THRESH_BINARY)
            sufix.append(f'_3_C_hom_lp{lpf}_hp{hpf}')
            images.append(image_hom)
            # hom_hist = hist.histogram(image_hom)
            # sufix.append(f'_hom_lp{lpf}_hp{hpf}_hist')
            # images.append(hom_hist)
            # print(f'Homeom count: {hist.histogram_count(image_hom)}')

        # 3.4: Watershed

        # DFT e Gradiente
        perct = 1
        showfreq = True
        image_fs, magnitude, freq_mask = sd.highpass_dft(image_norm, radius_perct=perct, show_freq=showfreq)
        sufix.append(f'_3_1_dftHPFr{perct}')
        images.append(image_fs)
        cv.imwrite(f'{outputfolder}{subfolder}{file}_3_1_dftHPFr{perct}_MASK.tiff', dft.show_dft(freq_mask))
        if showfreq:
            cv.imwrite(f'{outputfolder}{subfolder}{file}_3_1_dftHPFr{perct}_MAGN.tiff', magnitude)
        kernel_size = 3
        gradient_kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size,kernel_size))
        image_fs_grad = cv.morphologyEx(image_fs, cv.MORPH_GRADIENT, gradient_kernel, iterations=1)
        sufix.append(f'_3_2_dftHPFr{perct}_GRAD{kernel_size}')
        images.append(image_fs_grad)

        # 4.1: DFT e Canny
        image_fs_can = cv.Canny(image_fs_grad, threshold1=thr1, threshold2=thr2)
        sufix.append(f'_4_D_dftHPFr{perct}_CAN{thr1}_{thr2}')
        images.append(image_fs_can)
        # fs_zc_hist = hist.histogram(image_fs_can)
        # sufix.append(f'_dftHPFr{perct}_g{kernel_size}_CAN{thr1}_{thr2}')
        # images.append(fs_zc_hist)
        # print(f'DFT Canny count: {hist.histogram_count(image_fs_can.astype("uint8"))}')
        
        # Passo 4.2: DFT e Marr-Hildreth
        sigma = 1
        _, image_fs_zc = sd.edgesMarrHildreth(image_fs_grad, sigma)
        sufix.append(f'_4_E_dftHPFr{perct}_MH{sigma}')
        images.append(image_fs_zc)
        # fs_zc_hist = hist.histogram(image_fs_zc)
        # sufix.append(f'_dftHPFr{perct}_g{kernel_size}_MH{sigma}_hist')
        # images.append(fs_zc_hist)
        # print(f'DFT MH count: {hist.histogram_count(image_fs_zc.astype("uint8"))}')

        # Passo 4.3: DFT e Homeomorfico
        lpf = 0
        hpf = 4
        image_fs_hom = sd.homomorphic(image_fs_grad, lpf_const=lpf, hpf_const=hpf)
        # _, image_fs_hom = cv.threshold(image_fs_hom, 0, 255, cv.THRESH_BINARY)
        sufix.append(f'_4_F_dftHPFr{perct}_hom_lp{lpf}_hp{hpf}')
        images.append(image_fs_hom)
        # fs_hom_hist = hist.histogram(image_fs_hom)
        # sufix.append(f'_dftHPFr{perct}_g{kernel_size}_hom_lp{lpf}_hp{hpf}_hist')
        # images.append(fs_hom_hist)
        # print(f'DFT Homeom count: {hist.histogram_count(image_hom)}')

        for id, image in enumerate(images):
            cv.imwrite(outputfolder+subfolder+file+sufix[id]+fileformat, image)
    return

def dft_testing(imagefolder:str, files:List[str], fileformat:str, outputfolder:str):
    subfolder = 'testdft/'
    for file in files:
        kernel_size = 10
        gradient_kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size,kernel_size))
        image_orig = cv.imread(imagefolder+file+fileformat)
        # Passo 1: Obter canal Y
        image_Y = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
        # Passo 2: Normalizacao
        image_norm = hist.normalize(image_Y, alpha=0, beta=255)
        # DFT
        #image_dft = dft.my_dft(image_norm)
        for prct_radius in range(1,31,1):
            output, magnitude = sd.highpass_dft(image_norm, prct_radius)
            output = cv.morphologyEx(output, cv.MORPH_GRADIENT, gradient_kernel, iterations=1)
            cv.imwrite(outputfolder+subfolder+file+f"_DFTr{prct_radius}_k{kernel_size}"+fileformat, output)
    return

def generate_csv(file_rows:List[List],filepath:str) -> None:
    with open(filepath, 'w', newline='') as csv_file: # 'newline' arg needed because csvwriter adds extra '\n'
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(file_rows)

def approachA(image_orig, thr1:int = 0, thr2:int = 255):
    image_Y = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
    image_norm = hist.normalize(image_Y, alpha=0, beta=255)
    image_can = cv.Canny(image_norm, threshold1=thr1, threshold2=thr2)
    return image_can

def approachB(image_orig, sigma:float = 1.0):
    image_Y = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
    image_norm = hist.normalize(image_Y, alpha=0, beta=255)
    image_mh = sd.edgesMarrHildreth(image_norm, sigma=sigma)
    return image_mh

def main():
    image_folder = 'imagens/'
    output_folder = 'output/'
    filename = 'dive4_F3_master.mp4-00_00_00-00001_min'
    # files = ['dive4_F3_master.mp4-00_00_00-00001_min', 'dive4_F3_master.mp4-00_00_00-00002_min', 'dive4_F3_master.mp4-00_00_02-00005_min']
    # files = [
    #     'dive4_F3_master.mp4-00_00_00-00001_min', 
    #     'dive4_F3_master.mp4-00_00_00-00002_min', 
    #     'dive4_F3_master.mp4-00_00_02-00005_min',
    #     'vlcsnap-00_00_022022-04-17-18h24m07s235_min',
    #     'vlcsnap-00_00_062022-04-17-18h24m12s243_min',
    #     'vlcsnap-00_00_112022-04-17-18h24m14s189_min',
    #     'vlcsnap-00_00_152022-04-17-18h24m16s284_min',
    #     'vlcsnap-00_00_162022-04-17-18h24m19s150_min',
    #     'vlcsnap-00_00_192022-04-17-18h24m21s791_min',
    #     'vlcsnap-00_00_252022-04-17-18h24m25s457_min'
    #     ]
    
    # files = [
    #     'dive4_F3_master.mp4-00_00_00-00001_min', 
    #     'dive4_F3_master.mp4-00_00_00-00002_min', 
    #     'dive4_F3_master.mp4-00_00_02-00005_min',
    #     'vlcsnap-00_00_022022-04-17-18h24m07s235_min',
    #     'vlcsnap-00_00_062022-04-17-18h24m12s243_min',
    #     'vlcsnap-00_00_112022-04-17-18h24m14s189_min',
    #     'vlcsnap-00_00_152022-04-17-18h24m16s284_min',
    #     'vlcsnap-00_00_162022-04-17-18h24m19s150_min',
    #     'vlcsnap-00_00_192022-04-17-18h24m21s791_min',
    #     'vlcsnap-00_00_252022-04-17-18h24m25s457_min',
    #     'vlcsnap-00_00_162022-11-27-19h48m20s747_min',
    #     'vlcsnap-00_00_202022-11-27-19h48m22s492_min',
    #     'vlcsnap-00_00_212022-11-27-19h48m24s625_min',
    #     'vlcsnap-00_00_232022-11-27-19h48m26s190_min',
    #     'vlcsnap-00_00_252022-04-17-18h24m25s457_min',
    #     'vlcsnap-00_00_262022-11-27-19h48m27s519_min',
    #     'vlcsnap-00_00_272022-11-27-19h48m29s454_min',
    #     'vlcsnap-00_00_292022-11-27-19h48m31s290_min'
    #     ]

    file_format = '.png'
    
    # colourspace_func(image_folder, files, file_format, output_folder)
    # histogram_func(image_folder, files, file_format, output_folder)
    # spacedomain_func(image_folder, files, file_format, output_folder, lpf=0, hpf=1, show_steps=False, show_magnitude=False)

    # testing_transformation(image_folder, files, file_format, output_folder)
    # contour_counting(image_folder, files, file_format, output_folder)
    # dft_testing(image_folder, files, file_format, output_folder)

    img = cv.imread(f'{image_folder}\{filename}{file_format}')
    img_A_1 = approachA(img,0,100)
    img_A_2 = approachA(img,100,200)
    img_A_3 = approachA(img,0,255)
    cv.imwrite(f'{output_folder}\{filename}_approachA_0_100{file_format}', img_A_1)
    cv.imwrite(f'{output_folder}\{filename}_approachA_100_200{file_format}', img_A_2)
    cv.imwrite(f'{output_folder}\{filename}_approachA_0_255{file_format}', img_A_3)
    return

if __name__ == "__main__":
    main()