import numpy as np
from skimage import data, color, img_as_ubyte
from skimage.feature import canny
from skimage.transform import hough_ellipse
from skimage.draw import ellipse_perimeter
import cv2 as cv
import dft


def highpass_dft(image, radius_perct=1, show_freq = False):
    mask = dft.circular_mask(image.shape, radius_perct)
    freq_domain = dft.my_dft(image)
    magnitude = None
    if show_freq:
        magnitude = dft.show_dft(freq_domain)
    filt_img = freq_domain * mask
    return dft.invert_dft(filt_img), magnitude, filt_img

def lowpass_dft(image, inner_perct=1, outer_perct=100, show_freq = False):
    mask = dft.ring_mask(image.shape, inner_perct, outer_perct)
    freq_domain = dft.my_dft(image)
    magnitude = None
    if show_freq:
        magnitude = dft.show_dft(freq_domain)
    filt_img = freq_domain * mask
    return dft.invert_dft(filt_img), magnitude
    
def homomorphic(image, lpf_const = 1, hpf_const = 1, show_steps=False, show_dft=False,file_output = ''):
    if lpf_const < 0 or hpf_const < 0:
        print(f'Low-Pass and High-Pass constants must both be >= 0')
        return None
    magnitude = None

    # Gaussian and Laplacian arguments
    depth = cv.CV_8U # Gradação tonal
    ksize_width = int(image.shape[0]/10)
    if ksize_width % 2 != 1:
        ksize_width += 1
    ksize_height = int(image.shape[1]/10)
    if ksize_height % 2 != 1:
        ksize_height += 1
    ksize = (ksize_width, ksize_height)
    std_dev_x = 0
    # Generic arguments
    size = (image.shape[0], image.shape[1])
    mat_ones = np.ones(size, dtype=image.dtype)
    cv.add(mat_ones, image, mat_ones)
    img_log = np.log(mat_ones).astype("uint8")
    images = []

    #LPF
    lp = cv.GaussianBlur(img_log, ksize, std_dev_x)
    # lp, magnitude = lowpass_dft(img_log, show_freq=show_dft)
    
    lp_c = lpf_const * lp
    #lp_c = lp_c.astype('uint8')

    #HPF
    hp = cv.Laplacian(img_log, depth)
    # hp, _ = highpass_dft(img_log)
    
    hp_c = hpf_const * hp
    #hp_c = hp_c.astype('uint8')
    
    res = np.zeros(size, dtype=image.dtype)
    cv.add(lp_c, hp_c, res)
   
    if show_steps:
        images = [lp, lp_c, hp, hp_c]
        title = ['LPF', 'LPF*const', 'HPF', 'HPF*const']
        for id, img in enumerate(images):
            cv.imshow(title[id], img)
            cv.waitKey(0)
        cv.destroyAllWindows()
    
    if show_dft and magnitude is not None:
        cv.imwrite(f"{file_output}_magnitude.tiff", magnitude)

    if file_output != '':
        images = [lp, lp_c, hp, hp_c]
        title = ['LPF', 'LPF_const', 'HPF', 'HPF_const']
        for id, img in enumerate(images):
            cv.imwrite(f"{file_output}_{title[id]}.png", img)

    return  cv.exp(res.astype("float32")).astype("uint8")

def edgesMarrHildreth(img, sigma):
    """
    finds edges using Marr-Hildreth edge detection
    parameters:
        img : input image
        sigma : standard deviation of gaussian
    output:
        binary edge image
    """
    
    
    size = int(2*(np.ceil(3*sigma))+1)

    x, y = np.meshgrid(np.arange(-size/2+1, size/2+1),
                       np.arange(-size/2+1, size/2+1))

    normal = 1 / (2.0 * np.pi * sigma**2)
    kernel = ((x**2 + y**2 - (2.0*sigma**2)) / sigma**4) * np.exp(-(x**2+y**2) / (2.0*sigma**2)) / normal  # Laplacian of Gaussian filter

    kern_size = kernel.shape[0]
    log = np.zeros_like(img, dtype=float)

    # applying filter
    for i in range(img.shape[0]-(kern_size-1)):
        for j in range(img.shape[1]-(kern_size-1)):
            window = img[i:i+kern_size, j:j+kern_size] * kernel
            log[i, j] = np.sum(window)

    log = log.astype(np.int64, copy=False)

    zero_crossing = np.zeros_like(log)

    # Zero-Crossing
    for i in range(log.shape[0]-(kern_size-1)):
        for j in range(log.shape[1]-(kern_size-1)):
            if log[i][j] == 0:
                if (log[i][j-1] < 0 and log[i][j+1] > 0) or (log[i][j-1] < 0 and log[i][j+1] < 0) or (log[i-1][j] < 0 and log[i+1][j] > 0) or (log[i-1][j] > 0 and log[i+1][j] < 0):
                    zero_crossing[i][j] = 255
            if log[i][j] < 0:
                if (log[i][j-1] > 0) or (log[i][j+1] > 0) or (log[i-1][j] > 0) or (log[i+1][j] > 0):
                    zero_crossing[i][j] = 255

    return log, zero_crossing

def hough_transform(canny, acc:int=20, thresh:int=250, min:int=100, max:int=150):
    '''
    input:
        canny - image already converted to grayscale and applied some form of edge detection
    '''
    # https://scikit-image.org/docs/stable/auto_examples/edges/plot_circular_elliptical_hough_transform.html

    # from skimage import data, color, img_as_ubyte
    # from skimage.feature import canny

    result = hough_ellipse(canny, accuracy=acc, threshold=thresh, min_size=min, max_size=max)

    return

def main():
    image_folder = 'imagens/'
    output_folder = 'output/spacedomain/'
    files = ['dive4_F3_master.mp4-00_00_00-00001','dive4_F3_master.mp4-00_00_00-00001_min']
    fileformat = '.png'

    for file in files:
        image_orig = cv.imread(image_folder+file+fileformat)
        image_gray = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
        _, mh = edgesMarrHildreth(image_gray, sigma=1.0)
        cv.imwrite(f"{output_folder}{file}_MH{fileformat}",mh)
    return
if __name__ == '__main__':
    main()