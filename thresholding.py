import numpy as np
import cv2 as cv
from skimage.feature import peak_local_max
from skimage.segmentation import watershed
from scipy import ndimage
from numpy import ones, zeros, unique, uint8

def niblack(image, max_value:int=255, block_size:int=3, k:float=0):
    nib = cv.ximgproc.niBlackThreshold(
                _src=image,
                maxValue=max_value,
                type=cv.THRESH_BINARY,
                blockSize=block_size,
                k=k
            )
    return nib

def adaptive_gauss(image, max_value:int=255, block_size:int=5, const=1):
    return cv.adaptiveThreshold(image, max_value, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, block_size, const)

def adaptive_mean(image, max_value:int=255, block_size:int=5, const=1):
    return cv.adaptiveThreshold(image, max_value, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, block_size, const)

def otsu(image, thresh:int=0, max_value:int=255):
    # _, thresh = cv.threshold(image, thresh, max_value, cv.THRESH_OTSU)
    _, thresh = cv.threshold(image, thresh, max_value, cv.THRESH_BINARY + cv.THRESH_OTSU)
    return thresh

def fill_holes(img_thresh, threshold=1000):
    contours, _ = cv.findContours(img_thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    holes=[]
    for con in contours:
        area = cv.contourArea(con)
        if area < threshold:
            holes.append(con)
    cv.drawContours(img_thresh, holes, -1, 255, -1)
    return img_thresh

def wtrshed(gray, otsu_min:int=0, otsu_max:int=255, min_dist:int=20):
    # https://stackoverflow.com/questions/57813137/how-to-use-watershed-segmentation-in-opencv-python
    '''
    parameters:
        img - grayscale image
    '''
    thresh = otsu(gray, thresh=otsu_min, max_value=otsu_max)
    # thresh = fill_holes(thresh)
    distance_map = ndimage.distance_transform_edt(thresh)
    local_max = peak_local_max(distance_map, min_distance=min_dist, labels=thresh)

    markers = ndimage.label(local_max, structure=ones((3, 3)))[0]
    labels = watershed(-distance_map, markers, mask=thresh)
    print(labels.shape)
    total_area = 0
    output = zeros(gray.shape, dtype="uint8")
    for label in unique(labels):
        if label == 0:
            continue

        # Create a mask
        mask = zeros(gray.shape, dtype="uint8")
        mask[labels == label] = 255

        # Find contours and determine contour area
        cnts = cv.findContours(mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        c = max(cnts, key=cv.contourArea)
        area = cv.contourArea(c)
        total_area += area
        cv.drawContours(output, [c], -1, (36,255,12), 4)
    return output

def cv_watershed(gray, otsu_min:int=0, otsu_max:int=255):
    
    output = zeros(gray.shape, dtype="uint8")
    ret, thresh = cv.threshold(gray,thresh=otsu_min,maxval=otsu_max, type=cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    # noise removal
    kernel = ones((3,3),uint8)
    opening = cv.morphologyEx(thresh,cv.MORPH_OPEN,kernel, iterations = 2)
    # sure background area
    sure_bg = cv.dilate(opening,kernel,iterations=3)
    # Finding sure foreground area
    dist_transform = cv.distanceTransform(opening,cv.DIST_L2,5)
    ret, sure_fg = cv.threshold(dist_transform,0.7*dist_transform.max(),255,0)
    # Finding unknown region
    sure_fg = uint8(sure_fg)
    unknown = cv.subtract(sure_bg,sure_fg)

    # Marker labelling
    ret, markers = cv.connectedComponents(sure_fg)
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1
    # Now, mark the region of unknown with zero
    markers[unknown==255] = 0
    wtr = markers.copy()
    for i in range(0, markers.shape[0]):
        for j in range(0, markers.shape[1]):
            if markers[i][j] > 1:
                try:
                    for y in range(-1,2):
                        for x in range(-1,2):
                            wtr[i+x][j+y] = markers[i][j]
                except:
                    pass
    #out_markers = cv.watershed(gray,markers)
    # output[out_markers==-1] = [0,0,0]
    return wtr

def main():
    image_folder = 'imagens/'
    output_folder = 'output/thresholding/'
    files = ['dive4_F3_master.mp4-00_00_00-00001','dive4_F3_master.mp4-00_00_00-00001_min']
    fileformat = '.png'
    for file in files:
        image_orig = cv.imread(image_folder+file+fileformat)
        image_gray = cv.cvtColor(image_orig, cv.COLOR_BGR2GRAY)
        min_dists = [x for x in range(10,110,10)]
        kernel = np.ones((4,4),np.uint8)
        # thresh = otsu(image_gray)
        # cv.imwrite(f"{output_folder}{file}_otsu{fileformat}", thresh)
        
        # for md in min_dists:
        #     img_out = wtrshed(image_gray, 0, 255, min_dist=md)
        #     cv.imwrite(f"{output_folder}_{md}{fileformat}", img_out)
        
        # img_out = cv_watershed(image_gray)
        # cv.imwrite(f"{output_folder}{files[0]}_watershed_cv{fileformat}", img_out)

        # image_can = cv.Canny(image_gray, threshold1=100, threshold2=200)
        # cv.imwrite(f"{output_folder}{file}_CANNY{fileformat}", image_can)
        erosion = cv.erode(image_gray,kernel,iterations = 1)
        cv.imwrite(f"{output_folder}{file}_EROSION{fileformat}", erosion)
        dilation = cv.dilate(image_gray,kernel,iterations = 1)
        cv.imwrite(f"{output_folder}{file}_DILATION{fileformat}", dilation)
        opening = cv.morphologyEx(image_gray, cv.MORPH_OPEN, kernel)
        cv.imwrite(f"{output_folder}{file}_OPEN{fileformat}", opening)
        closing = cv.morphologyEx(image_gray, cv.MORPH_CLOSE, kernel)
        cv.imwrite(f"{output_folder}{file}_CLOSE{fileformat}", closing)

    return

if __name__ == '__main__':
    main()