import numpy as np
from matplotlib import pyplot as plt
import cv2 as cv
import io               #  Necessary for storing plt image in a variable
from PIL import Image   #  ditto

# Testar histograma adaptativo
def histogram(image, num_of_bins=256, range=[0,256], title=''):
    #https://dev-qa.com/2003653/matplotlib-how-to-save-the-graph-to-a-variable
    plt.hist(image.ravel(), num_of_bins, range)
    buf = io.BytesIO()
    plt.title(title)
    plt.savefig(buf, format='png')
    plt.clf()
    buf.seek(0)
    return cv.UMat(np.array(Image.open(buf), dtype=np.uint8)) #  Needed to convert PIL format to UMat

def histogram_count(image, hist_size=[256], range=[0,256]):
    hist = cv.calcHist([image], [0], None, hist_size, range)
    dict = {}
    #print(hist)
    for i,_ in enumerate(hist):
        if hist[i][0] != 0:
            dict[i] = hist[i][0]
    return dict

def histogram_255_count(image, hist_size=[256], range=[0,256]):
    hist = cv.calcHist([image], [0], None, hist_size, range)
    return int(hist[255][0])

def normalize(image, alpha=0, beta=255):
    image_normalized = np.zeros(image.shape)
    image_normalized = cv.normalize(image, image_normalized, alpha, beta, cv.NORM_MINMAX)

    return image_normalized

def equalization(image):
    if len(image.shape) > 2:
        image_grayscale = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    else:
        image_grayscale = image
    eq = cv.equalizeHist(image_grayscale)

    return eq

def clahe(image, clip_limit=1, tile_size=(5,5)):
    if len(image.shape) > 2:
        image_grayscale = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    else:
        image_grayscale = image
    
    clahe = cv.createCLAHE(clipLimit=clip_limit, tileGridSize=tile_size)
    return clahe.apply(image_grayscale)

def main():
    image_folder = 'imagens/'
    output_folder = 'output/histogram/'
    files = ['dive4_F3_master.mp4-00_00_00-00001','dive4_F3_master.mp4-00_00_00-00001_min']
    fileformat = '.png'

    for file in files:
        img = cv.imread(image_folder+file+fileformat)
        if img is None:
            print(f"Imagem nao encontrada")
            return
        hist = histogram(img)
        cv.imwrite(f"{output_folder}{file}_HIST{fileformat}",hist)

        eq = equalization(img)
        hist = histogram(eq)
        cv.imwrite(f"{output_folder}{file}_EQ{fileformat}",eq)
        cv.imwrite(f"{output_folder}{file}_EQ_HIST{fileformat}",hist)

        norm = normalize(img)
        hist = histogram(norm)
        cv.imwrite(f"{output_folder}{file}_NORM{fileformat}",norm)
        cv.imwrite(f"{output_folder}{file}_NORM_HIST{fileformat}",hist)

        cl = clahe(img,2,(36,36))
        hist = histogram(cl)
        cv.imwrite(f"{output_folder}{file}_CLAHE{fileformat}",cl)
        cv.imwrite(f"{output_folder}{file}_CLAHE_HIST{fileformat}",hist)
    return

if __name__ == '__main__':
    main()